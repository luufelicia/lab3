package dawson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    @Test
    public void shouldReturnParameter()
    {
        assertEquals("testing that echo method returns 5", 5, App.echo(5));
    }

    @Test
    public void testMethodOneMore()
    {
        assertEquals("testing that oneMore returns one number bigger than input", 6, App.oneMore(5));
    }
}
